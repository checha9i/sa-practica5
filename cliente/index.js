const axios = require("axios");
const readline = require("readline");

function replDemo() {
  var state = 0;
  return new Promise(function (resolve, reject) {
    let rl = readline.createInterface(process.stdin, process.stdout);
    if (state == 0)
      rl.setPrompt(
        "Seleccione una opcion \n " +
          "1. Solicitar pedido \n" +
          "   Ingrese su pedido \n" +
          "2. Verificar pedido \n" +
          "   Ingrese el numero de pedido \n" +
          "3. Verificar pedido \n" +
          "   Ingrese el numero de pedido \n" +
          "4. Salir\n"
      );
    else if (state == 1) rl.setPrompt("Ingrese Pedido:");
    else if (state == 2) rl.setPrompt("Ingrese numero de pedido");
    else if (state == 3) rl.setPrompt("Ingrese numero de pedido");

    rl.prompt();

    rl.on("line", function (line) {
      if (state == 0) {
        if (line === "1") {
          state = 1;
        } else if (line === "2") {
          state = 2;
        } else if (line === "3") {
          state = 3;
        } else if (line === "4") {
          rl.close();
          return; // bail here, so rl.prompt() isn't called again
        } else {
          state = 0;
        }
      } else if (state == 1) {
        let body = {
          pedido: line,
        };
        //            console.log(body);
        axios
          .post("http://localhost:3002/SolicitarPedido", body, {
            headers: {
              "Content-Type": "application/json",
            },
          })
          .then((res) => {
            console.log("\n Su numero de pedido es: ");
            console.log(res.data.id);
          })
          .catch((err) => {
            console.log(err);
          });
        console.log(`Se ha solicitado el pedido: ${line}`);
        state = 0;
      } else if (state == 2) {
        console.log(`Estado del pedido ${line}: `);
        let body = {
          id: parseInt(line),
        };
        axios
          .post("http://localhost:3002/VerificarEstadoRestaurante", body)
          .then((res) => {
            console.log("\n Su pedido esta: ");
            console.log(res.data.Estado);
          })
          .catch((err) => {
            console.log(err);
          });
        state = 0;
      } else if (state == 3) {
        let body = {
          id: parseInt(line),
        };
        console.log(`Estado del pedido ${line}: `);
        axios
          .post("http://localhost:3002/VerificarEstadoRepartidor", body)
          .then((res) => {
            console.log("\n Su pedido esta: ");
            console.log(res.data.Estado);
          })
          .catch((err) => {
            console.log(err);
          });
        state = 0;
      } else {
        state = 0;
      }

      rl.prompt();
    }).on("close", function () {
      console.log("bye");
      resolve(42); // this is the final result of the function
    });
  });
}

async function run() {
  try {
    let replResult = await replDemo();
    //console.log("repl result:", replResult);
  } catch (e) {
    console.log("failed:", e);
  }
}

run();
