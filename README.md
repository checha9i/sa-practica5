# SA-Practica5


## Como Utilizar
Aplica para todas las partes:

- Abrir la carpeta especifica para cada aplicacion
- ejecutar:
```bash
npm install
npm start
```
Cabe mencionar que las aplicaciones deben ser usadas en distintas consolas para ver el flujo completo

## Gulp
Con el siguiente comando lograremos armar nuestro artefacto, el cual encapsula nuestro codigo y dependencias
gulp --gulpfile services/gulpfile.js

## releases

https://gitlab.com/checha9i/sa-practica5/-/tags